#include <CoOS.h>			              /*!< CoOS header file	         */
#include <stdio.h>
#include "stm32f4xx_conf.h"
#include "LCD.h"
#include "LedTask.h"
#include "BSP.h"



#define STACK_SIZE_LCD 1024              /*!< Define "taskA" task size */
OS_STK     LCD_stk[2][STACK_SIZE_LCD];	  /*!< Define "taskA" task stack */




void LCDManagerTask (void* pdata);
void LCDHelloWorldTask(void * parg);
void LCDGradientTask(void * parg);
void LCDDrawAreaTask(void * parg);
void LCDScopeTask(void * parg);

#define STACK_SIZE_LCD 1024              /*!< Define "taskA" task size */
OS_STK     LCD_stk[2][STACK_SIZE_LCD];	  /*!< Define "taskA" task stack */

void CreateLCDTask(void){

	Init_AnalogJoy();

	LCD_Initialization();
	LCD_Clear(Blue);

	CoCreateTask (LCDManagerTask,0,1,&LCD_stk[0][STACK_SIZE_LCD-1],STACK_SIZE_LCD);

}

void LCDManagerTask (void* pdata) {

	OS_TID lcdId;


  for (;;) {

	  lcdId =	CoCreateTask (LCDHelloWorldTask,0,1,&LCD_stk[1][STACK_SIZE_LCD-1],STACK_SIZE_LCD);
	  waitForKey(5,0);
	  CoDelTask(lcdId);

	  lcdId =	CoCreateTask (LCDGradientTask,0,1,&LCD_stk[1][STACK_SIZE_LCD-1],STACK_SIZE_LCD);
	  waitForKey(5,0);
	  CoDelTask(lcdId);

	  lcdId =	CoCreateTask (LCDDrawAreaTask,0,1,&LCD_stk[1][STACK_SIZE_LCD-1],STACK_SIZE_LCD);
	  waitForKey(5,0);
	  CoDelTask(lcdId);

	  lcdId =	CoCreateTask (LCDScopeTask,0,1,&LCD_stk[1][STACK_SIZE_LCD-1],STACK_SIZE_LCD);
	  waitForKey(5,0);
	  CoDelTask(lcdId);


  }
}


coche(int desp, int i){
	char str[32];
	LCD_Clear(Red);
	LCD_PrintText(10,20, "Hola Mundo!", Blue, White);

	int r1 = 100+desp; int leng1 = 100;
	int r2 = 150+desp;int leng2 = 30;
	int r3 = 155+desp;int leng3 = 20;
		if(r1<0){
			leng1 =100+r1;
			r1 =0;
		}if(r2<0){
			leng2 =30+r2;
			r2 =0;
		}if(r3<0){
			leng3 = 20+r3;
			r3 =0;
		}
if(r1+leng1>=0)LCD_DrawRectangle(r1,100,leng1,20,1,Yellow);
if(r2+leng2>=0)	LCD_DrawRectangle(r2,80,leng2,20,1,Yellow);
if(r3+leng3>=0)	LCD_DrawRectangle(r3,85,leng3,10,2,Blue);
	LCD_FillCircle(120+desp,120,10,Black);
	LCD_FillCircle(180+desp,120,10,Black);
	LCD_FillCircle(120+desp,120,5,White);
	LCD_FillCircle(180+desp,120,5,White);
	sprintf(str,"Variable i: %d", i);
	LCD_PrintText(10,32,str,Blue,White);

	CoTimeDelay(0,0,0,500);
}
void LCDHelloWorldTask(void * parg){

	uint16_t i=0;

	int desp = 0;


	for(;;){
		coche(desp, i);
		desp=desp+10;
		if(desp==220){
			desp=-200;
		}
		i++;
	}

}


void LCDGradientTask(void * parg){


	LCD_Clear(Black);
	LCD_PrintText(10,224,"LCDGradientTask",White,Blue);


	for(;;){

		CoTimeDelay(0,0,0,500);
	}

}



void LCDDrawAreaTask(void * parg){

	LCD_Clear(Black);
	LCD_PrintText(10,224,"LCDDrawAreaTask",White,Blue);

	for(;;){
		CoTimeDelay(0,0,0,5);


	}


}


void LCDScopeTask(void * parg){

	LCD_Clear(Blue);
	LCD_PrintText(10,224,"LCDScopeTask",White,Black);

	for(;;){
		CoTimeDelay(0,0,0,10);

	}


}
